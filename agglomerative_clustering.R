# agglomerative single-linkage clustering (bottom-up cluster joining) with distance threshold
# join clusters based on minimum distance between members of clusters in an iterative approach 
# at each step the most similar clusters are merged
# the process is stopped as soon as the distance is greater than the distance threshold

# if distance matrix is made from relative pairwise distances with differing number of compared positions
# one sample might have small distances to two highly divering samples due to quality and coverage fluctuations
# -> the relative pairwise distance is no metric (triangle inequality is not satisfied)

# agglomerative single-linkage clustering is valid based on similarity but not in the context of epidemiological transmission analysis
# if check_transmission_validity is true, merging of distant samples by a "proxy" sample is detected and excluded
# clustering is repeated without those "proxy" samples until no such samples are detected any more

# result is provided in vector "groups" with group assignment for each sample of the distance matrix

####### PARAMETERS ########
# distmatrix: symmetric distance matrix (matrix or data.frame)
# threshold:  clustering threshold - only distance below threshold are considered for clustering (numeric)
# check_transmission_validity - detect proxy samples (bool)
##########################


agglomerative_clustering <- function(distmatrix, threshold, check_transmission_validity = TRUE){

  print("Start clustering")
  distm_forcluster <- distmatrix
  repeatclustering <- TRUE
  repeat_count = 1
  invalid_samples = c()
  invalid_samples_lastrounds = c() # avoid endless loops 
  
  while(repeatclustering){
    
    # get pairwise list of lower triangle of distance matrix, sort by distance to start agglomerative clustering
    m_pw = data.frame( t(combn(names(distm_forcluster),2)), dist=t(distm_forcluster)[lower.tri(distm_forcluster)] )
    m_pw = m_pw[order(m_pw[,3]),]
    m_pw[,1] <- as.character(m_pw[,1])
    m_pw[,2] <- as.character(m_pw[,2])
    
    # in the beginning no samples are grouped together
    groups <- rep("ungrouped", times=ncol(distmatrix))
    names(groups) <- rownames(distmatrix)
    
    # store information about invalidity for potenital following analyses
    groups[invalid_samples] = "proxy"
    
    invalid_samples_lastrounds = c(invalid_samples_lastrounds, invalid_samples)
    invalid_samples = c()
    
    # names for groups
    group_set = 1:ncol(distm_forcluster)
    
    # rowindex
    i = 1
    
    # initialize distance with smallest value
    dist = m_pw[1,3]
    
    while(dist < threshold){
      
      # current distance
      s1 = m_pw[i,1]
      s2 = m_pw[i,2]
      dist = m_pw[i,3]
      
      # get groups for samples
      g1 = groups[s1]
      g2 = groups[s2]
      
      # both samples have not been associated with any sample yet
      if (g1 == "ungrouped" && g2 == "ungrouped"){
        
        # assign cluster name and remove name from set
        new_group = group_set[1]
        group_set = group_set[-1]
        groups[c(s1, s2)] = new_group
      } 
      # one sample is grouped
      else if(g1 == "ungrouped" || g2 == "ungrouped"){
        
        if(g1 == "ungrouped"){
          new_s = s1
          grouped_s = s2
          group_members = names(groups[groups == g2])
        }else{
          new_s = s2
          grouped_s = s1
          group_members = names(groups[groups == g1])
        }
        
        # check distance between new sample and second-closest sample within existing group
        dist2 <- sort(distm_forcluster[new_s, group_members])[2]
        
        # for epidemiological clustering second-closest sample should be accessible in two "hops" within threshold
        if(check_transmission_validity && dist2 > threshold * 2 ){ 
          invalid_samples = c(invalid_samples, new_s)
          invalid_samples = c(invalid_samples, grouped_s)
        }else{
          # assign new sample existing group
          groups[new_s] = groups[grouped_s]
        }
      }# both samples are grouped but groups are different
      else if(g1 != g2){
        
        # choose smaller group name as new group name
        group_name = sort(c(g1, g2))[1]
        remove_group = sort(c(g1, g2))[2]
        
        group_members_min = names(groups[groups == group_name])
        group_members_max = names(groups[groups == remove_group])
        
        # check distance between both samples with second-closest sample of other group 
        dist2_group_name = sort(distm_forcluster[names(group_name), group_members_max])[2]
        dist2_remove_group = sort(distm_forcluster[names(remove_group), group_members_min])[2]
        
        # for epidemiological clustering second-closest samples of both samples should be accessible in two "hops" within threshold
        if(check_transmission_validity && (dist2_remove_group > threshold*2 || dist2_group_name > threshold*2)){
          
          # add samples with excessive distance to "proxy" samples 
          invalid_samples = c(invalid_samples, names(remove_group), names(group_name))
          
        } else {
          
          # merge groups
          groups[group_members_max] = group_name
          
          # release unused group name for reuse
          group_set = unique(c(as.numeric(remove_group), group_set))
        }
      }
      i=i+1
    }
    
    # analyze potential "proxy" samples, remove one and repeat clustering (remove minimum number of samples from analysis)
    if(length(invalid_samples) > 0){
      
      # samples connecting diverse clusters are found several times (from different cluster)
      # exclude sample with minimum difference first - if count is equal remove sample with lower overall distance to all samples
      count_invalid_samples = table(invalid_samples)
      highest_count_invalid_sample = max(count_invalid_samples)
      invalid_samples = names(count_invalid_samples[count_invalid_samples == highest_count_invalid_sample])
      invalid_samples = names(sort(rowSums(distmatrix[unique(invalid_samples),]))[1])
      
      distm_forcluster <- distm_forcluster[!(rownames(distm_forcluster) %in% invalid_samples), !(colnames(distm_forcluster) %in% invalid_samples)]
      print(paste("Removing", invalid_samples[1]))
       
    }else{
      repeatclustering = FALSE
    }
    
    
  }
  groups[invalid_samples_lastrounds] = "proxy"
  return (groups)
}
